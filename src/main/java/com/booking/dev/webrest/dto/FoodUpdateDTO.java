package com.booking.dev.webrest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FoodUpdateDTO {

    private String categoryId;
    private String name;
    private Double price;
    private String description;
    private String pathImage;
    private Boolean available;

}