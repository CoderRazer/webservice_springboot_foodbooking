package com.booking.dev.webrest.v1;

import com.booking.dev.domain.Food;
import com.booking.dev.repository.FoodRepository;
import com.booking.dev.repository.custom.FoodRepositoryCustom;
import com.booking.dev.webrest.dto.FoodCreateDTO;
import com.booking.dev.webrest.dto.FoodDetailDTO;
import com.booking.dev.webrest.dto.FoodUpdateDTO;
import com.booking.dev.webrest.response.RestResponseUtils;
import io.vavr.collection.List;
import io.vavr.control.Option;
import io.vavr.control.Try;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

@CrossOrigin(origins = "http://localhost:4000")
@RestController
@RequestMapping("/api/v1")
public class FoodController {

    private final FoodRepository foodRepository;
    private final FoodRepositoryCustom foodRepositoryCustom;

    private final Logger logger = LoggerFactory.getLogger(FoodController.class);

    public FoodController(FoodRepository foodRepository,
                          FoodRepositoryCustom foodRepositoryCustom){
        this.foodRepository = foodRepository;
        this.foodRepositoryCustom = foodRepositoryCustom;
    }

    @GetMapping("/foods")
    public ResponseEntity<?> finAll() throws URISyntaxException {
        return new ResponseEntity<>(RestResponseUtils.create(
                Option.of(foodRepository.findAll()).toTry(),"GET LIST FOOD",200),HttpStatus.OK);
    }

    @GetMapping("/food:byCategory/{_idCategory}")
    public ResponseEntity<?> getByCategory(@PathVariable String _idCategory) throws URISyntaxException {

        val result = foodRepositoryCustom.getByCategory(_idCategory);
        return new ResponseEntity<>(RestResponseUtils.create(result,"GET FOOD BY CATEGORY SUCCESS",200), HttpStatus.OK);

    }

    @PostMapping("/food:create")
    public ResponseEntity<?> create(@Valid @RequestBody FoodCreateDTO dto) throws URISyntaxException {

        Try result = Option.of(foodRepository.save(new Food(dto.getCategoryId(),dto.getName(),dto.getPrice(),dto.getDescription(),dto.getPathImage(),dto.getAvailable()))).toTry();

        if (result.isSuccess()){
            return new ResponseEntity<>(RestResponseUtils.create(result,"CREATE FOOD SUCCESS",200), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(RestResponseUtils.create(result,"CREATE FOOD FAILURE",500),HttpStatus.OK);
        }
    }


    @PutMapping("/food:update/{_id}")
    public ResponseEntity<?> update(@PathVariable String _id, @Valid @RequestBody FoodUpdateDTO dto) throws URISyntaxException {
        Try<Boolean> result = foodRepositoryCustom.update(_id,dto);

        if (result.isSuccess()){
            return new ResponseEntity<>(RestResponseUtils.create(result,"UPDATE FOOD SUCCESS",200),HttpStatus.OK);
        } else {
            return new ResponseEntity<>(RestResponseUtils.create(result,"UPDATE FOOD FAILURE",500),HttpStatus.OK);
        }

    }


}