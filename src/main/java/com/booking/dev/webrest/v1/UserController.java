package com.booking.dev.webrest.v1;

import com.booking.dev.domain.User;
import com.booking.dev.repository.UserRepository;
import com.booking.dev.repository.custom.UserRepositoryCustom;
import com.booking.dev.webrest.dto.UserRegisterDTO;
import com.booking.dev.webrest.dto.UserUpdateDTO;
import com.booking.dev.webrest.response.RestResponseUtils;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api/v1")
public class UserController {

    private final UserRepository userRepository;
    private final UserRepositoryCustom userRepositoryCustom;
    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    public UserController(UserRepository userRepository,
                          UserRepositoryCustom userRepositoryCustom){
        this.userRepository = userRepository;
        this.userRepositoryCustom = userRepositoryCustom;
    }

    @GetMapping("/users")
    public ResponseEntity<?> finAll() throws URISyntaxException {

        return new ResponseEntity<>(RestResponseUtils.create(
                Option.of(userRepository.findAll()).toTry(),"GET LIST USER",200), HttpStatus.OK);
    }

    @PostMapping("/user:register")
    public ResponseEntity<?> register(@Valid @RequestBody UserRegisterDTO dto) throws URISyntaxException {

        Try result = Option.of(userRepository.save(new User(dto.getName(),dto.getEmail(),dto.getPassword(),dto.getPhone(),dto.getAddress()))).toTry();

        if (result.isSuccess()){
            return new ResponseEntity<>(RestResponseUtils.create(result,"REGISTER SUCCESS",200),HttpStatus.OK);
        } else {
            return new ResponseEntity<>(RestResponseUtils.create(result,"REGISTER FAILURE",500),HttpStatus.OK);
        }
    }

    @GetMapping("/user/{_id}")
    public ResponseEntity<?> findOne(@PathVariable String  _id) throws URISyntaxException {
        return new ResponseEntity<>(RestResponseUtils.create(
                Option.of(userRepository.findById(_id)).toTry(),"GET ONE USER",200),HttpStatus.OK
        );
    }

    @PutMapping("/user:update/{_id}")
    public ResponseEntity<?> update(@PathVariable String _id, @Valid @RequestBody UserUpdateDTO dto) throws URISyntaxException {

        Try<Boolean> result = userRepositoryCustom.update(_id,dto);

        if (result.isSuccess()){
            return new ResponseEntity<>(RestResponseUtils.create(result,"UPDATE USER SUCCESS",200),HttpStatus.OK);
        } else {
            return new ResponseEntity<>(RestResponseUtils.create(result,"UPDATE USER FAILURE",500),HttpStatus.OK);
        }

    }



}
