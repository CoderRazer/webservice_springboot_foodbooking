package com.booking.dev.repository;

import com.booking.dev.domain.Category;
import io.vavr.control.Try;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends MongoRepository<Category,String> {

}