package com.booking.dev.repository;

import com.booking.dev.domain.ItemCart;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemCartRepository extends MongoRepository<ItemCart,String> {

}