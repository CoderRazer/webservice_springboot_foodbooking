package com.booking.dev.config;


/* This annotation will auto bin to prefix 'file' in application.properties
 * with this POJO Object when SpringBoot Started
 */

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration/* So that Spring create a Spring Bean in the application context */
@PropertySource("classpath:application.properties")
@ConfigurationProperties(prefix = "file")
public class PropertiesFileConfig {

    @Value("${file.upload-dir}")
    @Qualifier("food")
    private String uploadDir;

    public String getUploadDir() {
        return uploadDir;
    }

    public void setUploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
    }

}