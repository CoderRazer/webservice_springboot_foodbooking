package com.booking.dev.service;

import com.booking.dev.service.data.FileUploadInfo;
import io.vavr.control.Try;
import org.springframework.web.multipart.MultipartFile;

public interface FileUploadService {

    Try<FileUploadInfo> upLoadSingleFile(MultipartFile file , String nameFood);

}