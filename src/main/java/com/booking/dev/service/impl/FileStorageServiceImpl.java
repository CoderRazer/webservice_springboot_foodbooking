package com.booking.dev.service.impl;

import com.booking.dev.config.PropertiesFileConfig;
import com.booking.dev.exception.FileStorageException;
import com.booking.dev.service.FileStorageService;
import com.booking.dev.util.ConvertUnicodeUtil;
import com.booking.dev.util.UUIDUtil;
import io.vavr.control.Try;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


@Service
public class FileStorageServiceImpl implements FileStorageService {

    @Value("${path.access-resources-food}")
    private String PATH_ROOT;

    private final UUIDUtil uuidUtil;
    private final ConvertUnicodeUtil convertUnicodeUtil;
    private final PropertiesFileConfig propertiesFileConfig;
    private final Path fileStorageLocation;

    @Autowired
    public FileStorageServiceImpl(UUIDUtil uuidUtil,
                                  ConvertUnicodeUtil convertUnicodeUtil,
                                  PropertiesFileConfig propertiesFileConfig) {
        this.uuidUtil = uuidUtil;
        this.convertUnicodeUtil = convertUnicodeUtil;
        this.propertiesFileConfig = propertiesFileConfig;

        this.fileStorageLocation = Paths.get(propertiesFileConfig.getUploadDir()).toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        }
        catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.",ex);
        }
    }

    @Override
    public Try<String> storageFile(MultipartFile file, String nameFood) {

        if (file.isEmpty()) {
            return Try.failure(new Exception("File Not Contain Any Data"));
        }

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        if (fileName.contains("..")){
            return Try.failure(new Exception("File illegal"));
        }

        if (! checkTypeFile(getSuffixes(fileName))){
            return Try.failure(new Exception("File Format Invalid"));
        }

        String fileNameUUID = uuidUtil.generatorUUID() + "." + getSuffixes(fileName);
        nameFood = convertUnicodeUtil.removeAccent(nameFood);

        String pathSave = propertiesFileConfig.getUploadDir() + nameFood + "/img/";
        String pathEnd = nameFood + "/img/" + fileNameUUID;

        return Try.of(() -> {

            Path targetLocation = Paths.get(pathSave).resolve(fileNameUUID);
            Files.copy(file.getInputStream(),targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return PATH_ROOT + pathEnd;

        });
    }


    // TODO : =========================================== SUPPORT
    public String getSuffixes(String fileName) {
        String[] suf = fileName.split("\\.");
        return suf[(suf.length - 1)];// Type Format
    }

    public Boolean checkTypeFile(String type){
        return type.equalsIgnoreCase("png")
                || type.equalsIgnoreCase("jpg")
                || type.equalsIgnoreCase("jpeg");
    }

    public Boolean generateFolderFoodImage(String nameFood){

        String path = propertiesFileConfig.getUploadDir() + nameFood + "/img/";

        File directory = new File(path);
        if (!directory.exists()) {
            return directory.mkdirs();
        }
        return true;
    }


}
