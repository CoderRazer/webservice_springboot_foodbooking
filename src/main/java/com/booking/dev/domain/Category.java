package com.booking.dev.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "categorys")
@Data
@AllArgsConstructor
public class Category {

    @Id
    private String _id;

    @Indexed(unique = true)
    private String name;

    public Category(String name){
        this.name = name;
    }

}
