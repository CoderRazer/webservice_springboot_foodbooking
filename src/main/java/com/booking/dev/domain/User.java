package com.booking.dev.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
@Data
@AllArgsConstructor
public class User {

    @Id
    private String _id;
    private String name;

    @Indexed(unique = true)
    private String email;
    private String password;
    private String phone;
    private String address;

    public User(String name, String email, String password, String phone, String address){
        this.name = name;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.address = address;
    }
}
