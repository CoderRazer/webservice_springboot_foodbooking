package com.booking.dev.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "foods")
@Data
@AllArgsConstructor
public class Food {

    @Id
    private String _id;
    private String categoryId;

    @Indexed(unique = true)
    private String name;
    private Double price;
    private String description;
    private String pathImage;
    private Boolean available;

    public Food(String categoryId, String name, Double price, String description,String pathImage,Boolean available){
        this.categoryId = categoryId;
        this.name = name;
        this.price = price;
        this.description = description;
        this.pathImage = pathImage;
        this.available = available;
    }

}
