package com.booking.dev.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "itemcarts")
@Data
@AllArgsConstructor
public class ItemCart {

    @Id
    private String _id;
    private String orderId;
    private Food food;

    public ItemCart(String orderId, Food food){
        this.orderId = orderId;
        this.food = food;
    }

}
